<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ApiController extends Controller
{
    public function index() {

    	$dataDeviceId = DB::table('indicators')
    		->select('device_id')
    		// ->where()
            ->orderBy('device_id', 'asc')
            ->groupBy('device_id')
            ->get();

        // dd($dataDeviceId);

    	return view('form', compact('dataDeviceId'));
    }

    private function isDate($str) {
        return is_numeric(strtotime($str));
    }
    
    private function getDateNewFormat($oldDate) {
        // $old_date = date($oldDate);
        $middle = strtotime($oldDate);
        $new_date = date('Y-m-d H:i', $middle );

        return $new_date;
    }

    /**
     * 
     *
     * @return \Illuminate\Http\Response
     */
    public function getDataDevice(Request $request)
    {
        // очищаем папку с временными файлами 
        array_map('unlink', glob('tempFiles/*'));

        $patternDate = '/^[0-9]{2}[.]{1}[0-9]{2}[.]{1}[0-9]{4}$/';

        $data = null;

        if ($this->isDate($request->input('dateIntervalFrom')) && $this->isDate($request->input('dateIntervalTo'))) {

            $fromDate = $this->getDateNewFormat($request->input('dateIntervalFrom'));
            $toDate = $this->getDateNewFormat($request->input('dateIntervalTo'));
            
            // данные для файла из бд
            $data = DB::table('indicators')
                ->where([
                    ['device_id', '=', $request->input('device_id')],
                    ['timestamp', '>', $fromDate],
                    ['timestamp', '<', $toDate]
                ])               
                ->orderBy('timestamp', 'asc')
                ->get();
        } else {
            $data = [];
        }

        $fileName = 'tempFiles/' . $request->input('device_id') . time() . '.json';
        file_put_contents($fileName, json_encode($data));

        return response()->download($fileName);
    } 
}
