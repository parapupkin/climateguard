<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class IndicatorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   

    	$id = null;
    	if ($request->input('id')) {
    		$id = $request->input('id');
    	} else {
    		$id = 54565;
    	}
        // массив с данными для таблицы
        $dataForTable = [
            'temperature' => [
                'title' => 'Температура',                
                'value' => null
            ],
            'humidity' => [
                'title' => 'Влажность',                
                'value' => null
            ],
            'co2' => [
                'title' => 'CO2',                
                'value' => null
            ],
            'voc' => [
                'title' => 'VOC',                
                'value' => null
            ],
            'dust' => [
                'title' => 'Пыль',                
                'value' => null
            ],
            'noise' => [
                'title' => 'Шум',                
                'value' => null
            ],
            'vibration' => [
                'title' => 'Вибрация',                
                'value' => null
            ],
            'illumination' => [
                'title' => 'Освещенность',                
                'value' => null
            ],
            'light_pulsation' => [
                'title' => 'Пульсация света',                
                'value' => null
            ],
            'emp' => [
                'title' => 'ЭМИ',                
                'value' => null
            ]
        ];

        // данные для таблицы из бд
        $data = DB::table('indicators')
            ->orderBy('timestamp', 'desc')
            ->first();

        $arrayData = json_decode(json_encode($data), true); 

        foreach ($dataForTable as $dataKey => $value) {
            $dataForTable[$dataKey]['value'] = $arrayData[$dataKey];            
        }   

        $dataForCharts = DB::table('indicators')
            ->where('device_id', '=', $id)
            ->orderBy('timestamp', 'asc')
            ->get(); 

        $dataForChartsArray = [
            'dataForChartTemperature' => [],
            'dataForChartHumidity' => [],
            'dataForChartCo2' => [],
            'dataForChartVoc' => [],
            'dataForChartDust' => [],
            'dataForChartNoise' => [],
            'dataForChartVibration' => [],
            'dataForChartIllumination' => [],
            'dataForChartLight_pulsation' => [],
            'dataForChartEmp' => []
        ];
        // $dataForChartTemperature = [];

        foreach ($dataForCharts as $value) {
            array_push($dataForChartsArray['dataForChartTemperature'], [((int)strtotime($value->timestamp)*1000), floatval($value->temperature)]);  
            array_push($dataForChartsArray['dataForChartHumidity'], [((int)strtotime($value->timestamp)*1000),  floatval($value->humidity)]);   
            array_push($dataForChartsArray['dataForChartCo2'], [((int)strtotime($value->timestamp)*1000),  floatval($value->co2)]);   
            array_push($dataForChartsArray['dataForChartVoc'], [((int)strtotime($value->timestamp)*1000),  floatval($value->voc)]);   
            array_push($dataForChartsArray['dataForChartDust'], [((int)strtotime($value->timestamp)*1000),  floatval($value->dust)]);   
            array_push($dataForChartsArray['dataForChartNoise'], [((int)strtotime($value->timestamp)*1000),  floatval($value->noise)]);   
            array_push($dataForChartsArray['dataForChartVibration'], [((int)strtotime($value->timestamp)*1000),  floatval($value->vibration)]);   
            array_push($dataForChartsArray['dataForChartIllumination'], [((int)strtotime($value->timestamp)*1000),  floatval($value->illumination)]);   
            array_push($dataForChartsArray['dataForChartLight_pulsation'], [((int)strtotime($value->timestamp)*1000),  floatval($value->light_pulsation)]);   
            array_push($dataForChartsArray['dataForChartEmp'], [((int)strtotime($value->timestamp)*1000),  floatval($value->emp)]);                    
        } 

        // dd($dataForChartTemperature);       
       
        // dd($dataForCharts);
        return view('demo', compact('dataForTable', 'dataForChartsArray'));
    }

    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeDemo(Request $request)
    {
        // образец запроса

        //climateguard.ru/storeDemo?device_id=54565&temperature=45&humidity=25&co2=85&voc=45&dust=63&noise=33&vibration=345&illumination=44&light_pulsation=44&emp=78

        // проверяем, какой запрос пришел, если есть id устройства - записываем данные в бд
        if ($request->input('device_id')) {
            DB::insert('insert into indicators (device_id, temperature, humidity, co2, voc, dust, noise, vibration, illumination, light_pulsation, emp) values (?, ?, ?, ?, ?, ?,?, ?,?, ?, ?)', 
            [$request->input('device_id'),
             $request->input('temperature'),
             $request->input('humidity'),
             $request->input('co2'),
             $request->input('voc'),
             $request->input('dust'),
             $request->input('noise'), 
             $request->input('vibration'),
             $request->input('illumination'),
             $request->input('light_pulsation'),
             $request->input('emp')
            ]);    
        }        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
