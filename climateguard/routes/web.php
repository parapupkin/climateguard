<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('indicators', 'IndicatorController');

// getDataDevice
Route::get('api', 'ApiController@index')->name('apiIndex');

Route::post('getDataDevice', 'ApiController@getDataDevice')->name('getDataDevice');

Route::get('storeDemo', 'IndicatorController@storeDemo')->name('storeDemo');


