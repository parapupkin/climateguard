@extends('layouts.app')

@section('content')
<script type="text/javascript">
	const dataForChartTemperature = <?php echo json_encode($dataForChartsArray['dataForChartTemperature']) ?>;
	const dataForChartHumidity = <?php echo json_encode($dataForChartsArray['dataForChartHumidity']) ?>;
	const dataForChartCo2 = <?php echo json_encode($dataForChartsArray['dataForChartCo2']) ?>;
	const dataForChartVoc = <?php echo json_encode($dataForChartsArray['dataForChartVoc']) ?>;
	const dataForChartDust = <?php echo json_encode($dataForChartsArray['dataForChartDust']) ?>;
	const dataForChartNoise = <?php echo json_encode($dataForChartsArray['dataForChartNoise']) ?>;
	const dataForChartVibration = <?php echo json_encode($dataForChartsArray['dataForChartVibration']) ?>;
	const dataForChartIllumination = <?php echo json_encode($dataForChartsArray['dataForChartIllumination']) ?>;
	const dataForChartLight_pulsation = <?php echo json_encode($dataForChartsArray['dataForChartLight_pulsation']) ?>;
	const dataForChartEmp = <?php echo json_encode($dataForChartsArray['dataForChartEmp']) ?>;
	// console.log(Date.parse('2012-01-26T13:51:50.417Z'));
	// console.log(dataForChartTemperature);
</script>
<table>	
	<tr>
		<th>Показатель</th>
		<th>Текущее значение</th>
		<th>График изменения</th>	  
	</tr>
	@foreach ($dataForTable as $dataKey => $value)
	<tr>
		<td>{{ $dataForTable[$dataKey]['title'] }} </td>
		<td>{{ $dataForTable[$dataKey]['value'] }}</td>
		<td class="charts"><div id="{{ $dataKey }}">График</div></td>
	</tr>    
    @endforeach
</table>
@endsection