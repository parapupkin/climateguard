<!DOCTYPE html>
<!----><!--[if lt IE 9]><html class="IE-fix"><![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js no-js"><!--<![endif]-->
<head>
    @yield('title')
    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <meta name="description" content="Адаптер СМЭВ">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:title" content="Адаптер СМЭВ" />
    <meta property="og:type" content="article" />
    <meta property="og:image" content="" />
    <meta property="og:url" content="" />
    <meta property="og:description" content="" />

    <!-- <link rel="icon" type="image/png" href="{{ asset('img/portal_gerb.png') }}"> -->
   <!--  <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"> -->
    <!--
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    -->
    <link rel="stylesheet" href="{{ asset('css/main.css?1234567') }}">
    
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> -->    
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/highcharts-3d.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    @yield('charts')   
</head>

<body>
    <!--ALL-->
    <div class="jumbotron all">
        <div class="container-fluid wrapper-header">
            <!--HEADER-->
            <header>
                
            </header>
            <!--./HEADER-->
        </div>
        <div class="container-fluid wrapper-content">
            @yield('content')            
            <!--./CONTENT-->
        </div>
    </div>
    <!--ALL-->

    <!--Modals-->
    <!--./Modals-->
    <script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
    <!-- <script src="{{ asset('js/bootstrap.min.js') }}"></script> -->
    <script type="text/javascript" src="{{ asset('js/main.js?v=36ww4www3a27') }}"></script>
    
    @yield('script')    
</body>
</html>