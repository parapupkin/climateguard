<!doctype html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>ClimateGuard</title>

        <!-- Fonts -->
        <link href="../css/fonts/HelveticaBold.eot">

        <!-- Styles -->
        <style>

            @font-face {
                  font-family: "SF Display";
                /*  font-weight: 200;*/
                  src: url('../css/fonts/HelveticaBold.eot');
            }
            html, body {
                background-color: #fff;
                color: #636b6f;              
                min-height: 100vh;
                margin: 0;
            }

            .full-height {
                min-height: 100vh;
                background: url(../img/Main.jpg) no-repeat center center fixed;
                background-size: cover;
               /* background-repeat: ;*/
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
                font-family: 'Helvetica';
            }
            @media (min-width: 1000px) { 
                    .info {
                    position: absolute;
                    top: 50px;
                    right: 5%;
                    font-size: 1.5em;
                    color: black;
                    font-weight: bold;
                    max-width: 40%;
                    font-family: 'Helvetica';
                }
            }
            

            @media (max-width: 1000px) { 
                    .info {
                    position: absolute;
                    top: 20%;
                    right: 5px;
                    font-size: 1,3em;
                    color: black;
                    font-weight: bold;
                    max-width: 100%;
                    font-family: 'Helvetica';
                }    
            }

        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class='info'>
                <p>По вопросам партнерства<br/> и приобретения устройства обращаться:<br/>Владимир Ладыгин<br/>телефон/telegram: +7 (916) 325-41-10<br/>e-mail: vladygin@climateguard.ru<br/></p>                
            </div>       
        </div>
    </body>
</html>
