<!DOCTYPE html>
<html lang="ru" >
<head>
    <meta charset="UTF-8">
    <title>Api</title>  
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
    <link rel="stylesheet" href="{{ URL::asset('css/style_form.css') }}">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/prefixfree/1.0.7/prefixfree.min.js"></script>
</head>
<body>
    <div class="login">
	    <h1>Выберите id-устройства и временной интревал</h1>
	    <form method="post" action="{{ route('getDataDevice') }}">
	        {{ csrf_field() }}
	    	<input type="text" name="dateIntervalFrom" placeholder="От даты (формат дд.мм.гггг)" required="required" />
	        <input type="text" name="dateIntervalTo" placeholder="До даты (формат дд.мм.гггг)" required="required" />
	        <p>id-устройства</p>
	        <select name="device_id">
	         	@foreach ($dataDeviceId as $device_id)
	            <option value="{{ $device_id->device_id }}">{{ $device_id->device_id }}</option>
	            @endforeach
	        </select>	       
	        <button type="submit" class="btn btn-primary btn-block btn-large">Получить данные</button>
	    </form>
    </div>
    <!-- <script  src="js/index_form.js"></script> -->
</body>
</html>