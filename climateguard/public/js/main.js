let heightChart = '12%';

// console.log(window.matchMedia('(max-width: 1000px)').matches);

if (window.matchMedia('(max-width: 1000px)').matches) {
    heightChart = '40%';
}

if (window.matchMedia('(max-width: 500px)').matches) {
    heightChart = '100%';
}

// настройки для всех диаграмм
const defaultChart = {
    chartContent: null,
    highchart: null,
    defaults: {
        chart: {
            type: 'line',
            zoomType: 'x',
            height: heightChart,      
            backgroundColor: 'transparent'             
        },
        credits: {
            enabled: false
        },
        title: {        
            text: null         
        },
        xAxis: {
            type: 'datetime',
            dateTimeLabelFormats: { // don't display the dummy year
                month: '%e. %b',
                year: '%b'
            }
        },
        yAxis: {
            title: {
                text: null
            }
        },
        legend: {
            enabled: false
        },
        navigation: {
            buttonOptions: {
                enabled: false
            }
        },
        series: []
    },

    init: function(options) {

        this.highchart= jQuery.extend({}, this.defaults, options);
        this.highchart.chart.renderTo = this.chartContent;
    },

    create: function() {

        new Highcharts.Chart(this.highchart);
    }

};

// диаграмма температуры
let temperatureChart = {
    chartContent: 'temperature',
    options: {
        series: [{
            // type: 'line',    
            data: dataForChartTemperature
        }]
    }
};

// диаграмма влажности
let humidityChart = {
    chartContent: 'humidity',
    options: {
        series: [{
            // type: 'line',    
            data: dataForChartHumidity
        }]
    }
};

// диаграмма CO2
let co2Chart = {
    chartContent: 'co2',
    options: {
        series: [{
            // type: 'line',    
            data: dataForChartCo2
        }]
    }
};

// диаграмма VOC
let vocChart = {
    chartContent: 'voc',
    options: {
        series: [{
            // type: 'line',    
            data: dataForChartVoc
        }]
    }
};

// диаграмма пыли
let dustChart = {
    chartContent: 'dust',
    options: {
        series: [{
            // type: 'line',    
            data: dataForChartDust
        }]
    }
};

// диаграмма шума
let noiseChart = {
    chartContent: 'noise',
    options: {
        series: [{
            // type: 'line',    
            data: dataForChartNoise
        }]
    }
};

// диаграмма вибрации
let vibrationChart = {
    chartContent: 'vibration',
    options: {
        series: [{
            // type: 'line',    
            data: dataForChartVibration
        }]
    }
};

// диаграмма освещенности
let illuminationChart = {
    chartContent: 'illumination',
    options: {
        series: [{
            // type: 'line',    
            data: dataForChartIllumination
        }]
    }
};

// диаграмма световой пульсации
let light_pulsationChart = {
    chartContent: 'light_pulsation',
    options: {
        series: [{
            // type: 'line',    
            data: dataForChartLight_pulsation
        }]
    }
};

// диаграмма электромагн. импульса
let empChart = {
    chartContent: 'emp',
    options: {
        series: [{
            // type: 'line',    
            data: dataForChartEmp
        }]
    }
};

function generateCharts(nameChart) {
    
    // наслелуем свойства по умолчанию
    nameChart = jQuery.extend(true, {}, defaultChart, nameChart);

    // добавляем к новой диаграмме своих опций и создаем ее
    nameChart.init(nameChart.options);
    nameChart.create();
};

generateCharts(temperatureChart);
generateCharts(humidityChart);
generateCharts(co2Chart);
generateCharts(vocChart);
generateCharts(dustChart);
generateCharts(noiseChart);
generateCharts(vibrationChart);
generateCharts(illuminationChart);
generateCharts(light_pulsationChart);
generateCharts(empChart);
